package minesweeper;

import org.junit.Test;
import org.junit.*;
import org.junit.jupiter.api.RepeatedTest;

public class MineSweeperTest {

  @RepeatedTest(5)
  public void testWithDifferentSizes() throws Exception {
    Minesweeper minesweeper = new Minesweeper(5, 5, 0.65);
    minesweeper.startGame();

    Minesweeper minesweeper1 = new Minesweeper(10, 3, 0.3);
    minesweeper1.startGame();

    Minesweeper minesweeper2 = new Minesweeper(3, 25, 0.8);
    minesweeper2.startGame();
  }

  @Test
  public void testWithWrongProbability() throws Exception {
    Minesweeper minesweeper = new Minesweeper(5, 5, 25);

    Assert.assertTrue(minesweeper.booleanForTest);
  }


}
