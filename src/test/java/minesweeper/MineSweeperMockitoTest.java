package minesweeper;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

public class MineSweeperMockitoTest {

  @InjectMocks
  Minesweeper minesweeper;

  @Test
  public void mineSweeperTestWithWrongProb() {
    minesweeper = mock(Minesweeper.class);
    when(minesweeper.getProbability()).thenReturn(0.5);
    Assert.assertEquals(minesweeper.getProbability(), 0.5, 0);
  }
}
