package minesweeper;

import java.util.Random;

public class Minesweeper {

  private int rows;
  private int columns;
  private boolean[][] mineSweeperArrBoolean;
  private String[][] mineSweeperArrString;
  private double probabilityOfBombsSpawn;
  private static final double STANDART_PROBABILITY = 0.5;
  private static Random random;
  public boolean booleanForTest;

  public Minesweeper(int rows, int columns, double probabilityOfBombsSpawn) {
    this.rows = rows;
    this.columns = columns;
    if (probabilityOfBombsSpawn < 1 || probabilityOfBombsSpawn > 0) {
      this.probabilityOfBombsSpawn = probabilityOfBombsSpawn * 100;
    } else {
      booleanForTest = false;
      System.out.println("You entered wrong probability. Program will use standart");
      this.probabilityOfBombsSpawn = STANDART_PROBABILITY;
    }
    this.mineSweeperArrBoolean = new boolean[rows][columns];
    this.mineSweeperArrString = new String[rows][columns];
    random = new Random();

  }

  public double getProbability() {
    return probabilityOfBombsSpawn;
  }

  public void startGame() {
    createMineSweeperArrBoolean();
    printMineSweeperArrBoolean();
    System.out.println("\n\n");
    printMineSweeperArrString();

  }

  private void createMineSweeperArrBoolean() {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (random.nextInt(100) + 1 <= probabilityOfBombsSpawn) {
          mineSweeperArrBoolean[i][j] = true;
        } else {
          mineSweeperArrBoolean[i][j] = false;
        }
      }
    }
  }

  private void printMineSweeperArrBoolean() {
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (mineSweeperArrBoolean[i][j]) {
          System.out.print("*  ");
        } else {
          System.out.print(".  ");
        }
      }
      System.out.println();
    }
  }


  private void printMineSweeperArrString() {
    calculateMineSweeperArrString();
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        System.out.print(mineSweeperArrString[i][j] + "  ");
      }
      System.out.println();
    }
  }

  private void calculateMineSweeperArrString() {

    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (!mineSweeperArrBoolean[i][j]) {
          mineSweeperArrString[i][j] = countOfBombs(i, j);
        } else {
          mineSweeperArrString[i][j] = "*";
        }
      }
    }
  }

  private String countOfBombs(int row, int col) {
    int count = 0;
    if (!mineSweeperArrBoolean[row][col]) {
      if (row != 0 && mineSweeperArrBoolean[row - 1][col]) {
        count++;
      }
      if (row != (mineSweeperArrBoolean.length - 1) && mineSweeperArrBoolean[row + 1][col]) {
        count++;
      }
      if (col != 0 && mineSweeperArrBoolean[row][col - 1]) {
        count++;
      }
      if (col != (mineSweeperArrBoolean[0].length - 1) && mineSweeperArrBoolean[row][col + 1]) {
        count++;
      }
    }

    return Integer.toString(count);
  }
}
