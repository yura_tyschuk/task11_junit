package longest.plateau;

import java.util.ArrayList;

public class Test {

  public static void main(String[] args) {
    int[] array = new int[]{1, 2, 2, 2, 2, 3, 4, 4, 5, 6, 6, 6, 6, 7};
    ArrayList<Integer> list = new ArrayList<>();
    ArrayList<Integer> list2 = new ArrayList<>();

    int count = 0;
    int maxCount = 0;

    for (int i = 0; i < array.length; i++) {
      if (!list2.contains(array[i])) {
        for (int j = 0; j < array.length; j++) {
          if (array[i] == (array[j])) {
            count++;
          }
        }
        if (count > maxCount) {
          maxCount = count;
          list.clear();
          list.add(array[i]);
        }
        count = 0;
      }
    }
    for (int i = 0; i < maxCount; i++) {
      System.out.print(list.get(0) + " ");
    }
  }
}
