package longest.plateau;

public class Program {

  public static void main(String[] args) {
    int[] arr = new int[]{1, 2, 2, 2, 2, 3, 4, 4, 5, 6, 6, 6, 6, 7};
    LongestPlateau longestPlateau = new LongestPlateau(arr);
    longestPlateau.test();
  }
}
