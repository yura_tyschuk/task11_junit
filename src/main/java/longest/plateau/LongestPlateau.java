package longest.plateau;

import java.util.ArrayList;
import java.util.List;

public class LongestPlateau {

  private int[] arr;
  private List<Integer> list;
  private List<Integer> list2;

  public LongestPlateau(int[] arr) {
    this.arr = arr;
    list = new ArrayList<>();
    list2 = new ArrayList<>();
  }

  public void test() {
    int count = 0;
    int maxCount = 0;

    for (int i = 0; i < arr.length; i++) {
      if (!list2.contains(arr[i])) {
        for (int j = 0; j < arr.length; j++) {
          if (arr[i] == arr[j]) {
            count++;
          }
        }
        if (count > maxCount) {
          maxCount = count;
          list.clear();
          list.add(arr[i]);
        }
        count = 0;
      }
    }
    System.out.println(list);
  }
}
