package controller;

import minesweeper.Minesweeper;


public class Controller {

  Minesweeper minesweeper;

  public Controller() {
    minesweeper = new Minesweeper(5, 5, 0.3);
  }

  public void startGame() {
    minesweeper.startGame();
  }

  public double getProbability() {
    return minesweeper.getProbability();
  }
}
