package menu;

import controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "1 - Start minesweeper");
    menu.put("2", "2 - Get probability");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::startGame);
    methodsMenu.put("2", this::getProbability);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void startGame() {
    controller.startGame();
  }

  private double getProbability() {
    return controller.getProbability();
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
